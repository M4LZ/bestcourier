package com.bestcourier

case class CourierDetails (courierPerson : String, workStartTime :Int, workEndTime : Int, radiusService:Int, hasRefrigerator : Boolean,  charges : Double) {

}
